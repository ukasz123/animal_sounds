import 'dart:async';

import 'package:animal_sounds/data/data.dart';
import 'package:flutter/services.dart';
import 'package:soundpool/soundpool.dart';

class DataPreloader {
  final List<AnimalDataWrapper> wrappers;

  Future<bool> ready;

  DataPreloader(List<Animal> animals)
      : assert(animals != null),
        wrappers = animals
            .map((animal) => AnimalDataWrapper(animal))
            .toList(growable: false) {
    this.ready = Future
        .wait(wrappers.map((wrapper) => wrapper.loadSound(rootBundle)))
        .then((_) async => true);
  }
}

class AnimalDataWrapper {
  final Animal animal;
  int animalSound;

  AnimalDataWrapper(this.animal);

  Future loadSound(AssetBundle bundle) {
    if (animalSound == null || animalSound < 0) {
      if (animal.sound.source == Source.ASSETS) {
        return _loadFromAssets(bundle);
      } else {
        return _loadFromNetwork();
      }
    } else {
      return Future.value(null);
    }
  }

  Future _loadFromAssets(AssetBundle bundle) async {
//    print("Loading sound for ${animal.name} from ${animal.sound.path}");
    final soundData = await bundle.load(animal.sound.path);
//    print("Got file contents");
    this.animalSound = await Soundpool.load(soundData);
//    print("Sound ${animal.name} loaded: $animalSound");
    return;
  }

  Future _loadFromNetwork() async {
    this.animalSound = await Soundpool.loadUri(animal.sound.path);
    print("Sound ${animal.name} loaded: $animalSound");
  }
}
