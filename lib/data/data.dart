/// Describes how to get data of resource
class ResourceDescription {
  final Source source;
  final String path;

  const ResourceDescription(this.source, this.path)
      : assert(source != null),
        assert(path != null);
}

enum Source {
  /// should load from local assets
  ASSETS,

  /// should load from url
  NETWORK
}

class Animal {
  // TODO: multilanguage support
  final String name;
  final ResourceDescription image;
  final ResourceDescription sound;

  const Animal(this.name, this.image, this.sound)
      : assert(name != null),
        assert(image != null),
        assert(sound != null);
}
