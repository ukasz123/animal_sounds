import 'data.dart';

const String _pictureBase = "assets/pictures/";
const String _soundBase = "assets/sounds/";
const List<Animal> localDatabase = [
  const Animal(
    "Kot",
    const ResourceDescription(
      Source.ASSETS,
      _pictureBase + "cat.jpg",
    ),
    const ResourceDescription(
      Source.ASSETS,
      _soundBase + "cat.wav",
    ),
  ),
  const Animal(
    "Kurczak",
    const ResourceDescription(
      Source.ASSETS,
      _pictureBase + "chicken.jpg",
    ),
    const ResourceDescription(
      Source.ASSETS,
      _soundBase + "chicken.wav",
    ),
  ),
  const Animal(
    "Kogut",
    const ResourceDescription(
      Source.ASSETS,
      _pictureBase + "cock.jpg",
    ),
    const ResourceDescription(
      Source.ASSETS,
      _soundBase + "cock.wav",
    ),
  ),
  const Animal(
    "Pies",
    const ResourceDescription(
      Source.ASSETS,
      _pictureBase + "dog.jpeg",
    ),
    const ResourceDescription(
      Source.ASSETS,
      _soundBase + "dog.wav",
    ),
  ),
  const Animal(
    "Słoń",
    const ResourceDescription(
      Source.ASSETS,
      _pictureBase + "elephant.jpeg",
    ),
    const ResourceDescription(
      Source.ASSETS,
      _soundBase + "elephant.wav",
    ),
  ),
  const Animal(
    "Koń",
    const ResourceDescription(
      Source.ASSETS,
      _pictureBase + "horse.jpeg",
    ),
    const ResourceDescription(
      Source.ASSETS,
      _soundBase + "horse.wav",
    ),
  ),
  const Animal(
    "Lew",
    const ResourceDescription(
      Source.ASSETS,
      _pictureBase + "lion.jpg",
    ),
    const ResourceDescription(
      Source.ASSETS,
      _soundBase + "lion.wav",
    ),
  ),
  const Animal(
    "Sowa",
    const ResourceDescription(
      Source.ASSETS,
      _pictureBase + "owl.jpeg",
    ),
    const ResourceDescription(
      Source.ASSETS,
      _soundBase + "owl.wav",
    ),
  ),
  const Animal(
    "Gołąb",
    const ResourceDescription(
      Source.ASSETS,
      _pictureBase + "pidgeon.jpeg",
    ),
    const ResourceDescription(
      Source.ASSETS,
      _soundBase + "pidgeon.wav",
    ),
  ),
];
