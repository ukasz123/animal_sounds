import 'dart:math';

import 'package:animal_sounds/data/data.dart';
import 'package:animal_sounds/data/local_animals.dart';
import 'package:animal_sounds/services/data_preloader.dart';
import 'package:animal_sounds/widgets/ObjectProvider.dart';
import 'package:flutter/material.dart';
import 'package:soundpool/soundpool.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    DataPreloader _preloader = new DataPreloader(localDatabase);
    return ObjectProvider<DataPreloader>(
      data: _preloader,
      child: new MaterialApp(
        title: 'Picking game',
        theme: new ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: new MainPage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MainPage extends StatelessWidget {
  MainPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: FutureBuilder<bool>(
        builder: _buildBody,
        future: ObjectProvider.of<DataPreloader>(context).ready,
        initialData: false,
      ),
    );
  }

  Widget _buildBody(BuildContext context, AsyncSnapshot<bool> snapshot) {
    if (snapshot.hasData && snapshot.data) {
      DataPreloader preloader = ObjectProvider.of<DataPreloader>(context);
      return new LayoutBuilder(builder: (context, constraints) {
        final width = constraints.maxWidth;
        final height = constraints.maxHeight;
        final targetWidth = width / 3;
        final targetHeight = height / 3;
        final sourceSize = min(targetWidth, targetHeight);
        return new Stack(
          children: <Widget>[
            // drag targets
            Positioned(
              width: targetWidth,
              height: targetHeight,
              top: 0.0,
              left: 0.0,
              child: _buildTargetImage(preloader.wrappers[0]),
            ),
            Positioned(
              width: targetWidth,
              height: targetHeight,
              top: 0.0,
              right: 0.0,
              child: _buildTargetImage(preloader.wrappers[1]),
            ),
            Positioned(
              width: targetWidth,
              height: targetHeight,
              bottom: 0.0,
              left: 0.0,
              child: _buildTargetImage(preloader.wrappers[2]),
            ),
            Positioned(
              width: targetWidth,
              height: targetHeight,
              bottom: 0.0,
              right: 0.0,
              child: _buildTargetImage(preloader.wrappers[4]),
            ),
            // draggable element
            Positioned(
              top: height / 2,
              left: width / 2,
              child: FractionalTranslation(
                translation: Offset(-0.5, -0.5),
                child: Draggable<Animal>(
                  data: preloader.wrappers[2].animal,
                  child: new DraggableContents(
                    backgroundColor: Colors.deepOrange,
                    child: Text(
                      preloader.wrappers[2].animal.name,
                      style: TextStyle(fontSize: 34.0, color: Colors.white),
                    ),
                  ),
                  feedback: new DraggableContents(
                    backgroundColor: Colors.deepOrange.withOpacity(0.4),
                    child: Text(
                      preloader.wrappers[2].animal.name,
                      style: TextStyle(fontSize: 34.0, color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      });
    } else {
      return Container(
        width: double.infinity,
        height: double.infinity,
        child: Center(
          child: Column(
            children: <Widget>[
              Text("Loading sounds..."),
              CircularProgressIndicator(),
            ],
          ),
        ),
      );
    }
  }

  Widget _buildTargetImage(AnimalDataWrapper animal) {
    final ResourceDescription image = animal.animal.image;
    Widget imageWidget = null;
    if (image.source == Source.ASSETS) {
      imageWidget = Image.asset(image.path);
    } else if (image.source == Source.NETWORK) {
      imageWidget = Image.network(image.path);
    }
    return new DragTarget<Animal>(
      builder: (context, accepted, rejected) => imageWidget,
      onWillAccept: (draggedAnimal) => draggedAnimal.name == animal.animal.name,
      onAccept: (draggedAnimal) {
        Soundpool.play(animal.animalSound);
      },
    );
  }
}

class DraggableContents extends StatelessWidget {
  const DraggableContents({
    Key key,
    @required this.backgroundColor,
    @required this.child,
  }) : super(key: key);

  final Color backgroundColor;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: ShapeDecoration(
        shape: StadiumBorder(
          side: BorderSide(color: backgroundColor),
        ),
        color: backgroundColor,
        shadows: [
          BoxShadow(
            color: Colors.black26,
            offset: Offset(4.0, 4.0),
            blurRadius: 1.0,
          ),
        ],
      ),
      child: Padding(
        child: Material(color: Colors.transparent, child: child),
        padding: EdgeInsets.all(16.0),
      ),
    );
  }
}
