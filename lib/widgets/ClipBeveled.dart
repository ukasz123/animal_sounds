import 'package:flutter/widgets.dart';

class ClipBeveled extends StatelessWidget {
  final Widget child;

  final BeveledRectangleBorder _shape;

  ClipBeveled({
    Key key,
    this.child,
    Radius topLeft = Radius.zero,
    Radius topRight = Radius.zero,
    Radius bottomLeft = Radius.zero,
    Radius bottomRight = Radius.zero,
  })  : _shape = BeveledRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: topLeft,
                bottomLeft: bottomLeft,
                topRight: topRight,
                bottomRight: bottomRight)),
        super(key: key);

  const ClipBeveled.all({Key key, Widget child, Radius radius})
      : this(
          key: key,
          topLeft: radius,
          topRight: radius,
          bottomLeft: radius,
          bottomRight: radius,
          child: child,
        );

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      child: child,
      clipper: ShapeBorderClipper(shape: _shape),
    );
  }
}
