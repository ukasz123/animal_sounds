import 'package:flutter/widgets.dart';

class ObjectProvider<T> extends InheritedWidget {
  final T data;
  final Widget child;

  ObjectProvider({Key key, @required this.data, @required this.child})
      : assert(child != null),
        assert(data != null),
        super(key: key, child: child);

  ObjectProvider._typeOnly() : super();

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return !(oldWidget is ObjectProvider) ||
        ((oldWidget as ObjectProvider).data != this.data);
  }

  static T of<T>(BuildContext context) {
    final widget = context.inheritFromWidgetOfExactType(
        ObjectProvider._typeOnly<T>().runtimeType) as ObjectProvider<T>;
    return widget.data;
  }
}
