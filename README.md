# animal_sounds

A matching sound and image game

## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).


### Sources
Pictures were found on [www.pexels.com](https://www.pexels.com)

Sounds were found on [www.animal-sounds.org](http://www.animal-sounds.org/)